# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=8

DESCRIPTION="Download NHK foreign language lesson programs"
HOMEPAGE="https://riocampos-tech.hatenablog.com/entry/20200402/radirudegogaku"

SLOT="0"
KEYWORDS="amd64 x86"

IUSE="+gnutls"

RDEPEND="gnutls? ( media-video/ffmpeg[gnutls,network] )
		 !gnutls? ( media-video/ffmpeg[openssl,network] )"

src_unpack() {
	mkdir "${S}"
}

src_install() {
	into /usr
	newbin ${FILESDIR}/radirudegogaku-2408.rb radirudegogaku.rb
	insinto /etc
	doins "${FILESDIR}/radirudegogaku-cron.conf"
	fperms 0644 /etc/radirudegogaku-cron.conf

	insinto /etc/cron.daily
	newins ${FILESDIR}/radirudegogaku.cron radirudegogaku
	fperms 0755 /etc/cron.daily/radirudegogaku
}
