#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

require 'open-uri'
require 'optparse'
require 'pathname'
require 'fileutils'
require 'kconv'
require 'json'
require 'time'
require 'date'
$stdout.sync = true

WEEK_OF_THE_DAYS = %w[SU MO TU WE TH FR SA]
CORNER_REGEX = /[日月火水木金土]|[聴き]く$|ラジオアーカイブス|歴史再発見|芸術その魅力|文学の世界|科学と人間|漢詩をよむ/
ID_REGEX            = %r[((P\d{6}|\d{4}|F\d{3})_(?:\d\d))$]
ID_REGEX_WITH_INDEX = %r[((P\d{6}|\d{4}|F\d{3})_(?:\d\d))[^\d]+(\d+)$]
M4A = "-absf aac_adtstoasc -acodec copy"
MP3 = "-ab 64k -write_xing 0"

def check_ffmpeg_has_(regex)
  status = IO.popen('ffmpeg -version 2>&1') do |pipe|
    pipe.read
  end
  status[regex]
end

def is_win?
  RUBY_PLATFORM.downcase[/mswin(?!ce)|mingw|cygwin|bccwin/] ? true : false
end

def record_year
  @date.year
end

def time_counter_to_sec(counter)
  ((counter[0].to_i * 360000 + counter[1].to_i * 6000 + counter[2].to_i * 100 + counter[3].to_i) / 100.0).round
end

def progress_bar(progress, max, unit)
  max_digits = max.to_s.size
  print "\r" + "[#{'%-50s' % ('#' * (progress.to_f / max * 50).to_i)}] #{'%*d' % [max_digits, progress]} / #{max} #{unit}"
end

### ダウンロード進行状況の表示
def download(command_ffmpeg)
  duration = nil
  progress = 0
  counter_str = '(\d{2}):(\d{2}):(\d{2}).(\d{2})'
  line_buffer = ""
  received_error_message = false
  accept_to_quit = false
  cannot_start_error = false

  IO.popen(command_ffmpeg, "r+") do |pipe|

    pipe.each("r") do |line_raw|
      line = @is_win ? line_raw.tosjis : line_raw.toutf8

      ### ffmpeg が入力受付できる状況になった
      unless line.scan("[q]").empty?
        accept_to_quit = true
      end

      eachline_array = line.split(/\n|\r/)
      eachline_array_size = eachline_array.size

      eachline_array.each_with_index do |l, i|
        ln = ""
        if i == 0
          ln = line_buffer + l
        else
          ln = l
        end

        if eachline_array_size == i + 1
          if eachline_array_size == 1
            line_buffer = line_buffer + l
            next
          else
            line_buffer = l
          end
        end

        error_message_array = ln.scan(/^\[hls @ [^\]]+\] parse_playlist error|^\[crypto @ [^\]]+\] Unable to open resource/)
        unless error_message_array.empty?
          unless received_error_message
            case error_message_array.first
            when /parse_playlist/
              message = "　ダウンロードを開始出来ませんでした。"
              ### 入力受付できない状態でffmpegが終了するためメソッドを終了
              cannot_start_error = true
            when /Unable to open resource/
              message = "　ダウンロードファイルの一部を取得出来ませんでした。\n　ファイルが作成されているばあいには不完全ですので削除してください。"
            end
            puts " ！！！\n#{message}\n　スクリプトを再実行してください。"
            return if cannot_start_error
            received_error_message = true
          end
        end
      end

      ### エラー発生＆ffmpegに"q"を送って終了する
      if received_error_message && accept_to_quit
        pipe.puts "q"
        pipe.close_write
        return
      end

      counter_duration_match = line.scan(/: #{counter_str},/).flatten
      unless counter_duration_match == []
        duration = time_counter_to_sec(counter_duration_match)
      end
      counter_progress_match = line.scan(/time=#{counter_str}/).flatten
      unless counter_progress_match == []
        progress = time_counter_to_sec(counter_progress_match)
        progress = duration if duration && progress > duration
      end
      if duration && !received_error_message
        progress_bar(progress, duration, "秒")
      end
    end
    puts "完了"
  end
end

### 各番組タイトル取得
def fetch_title(lesson, corner, program_title_orig)
  lesson_title = lesson['file_title'] || ""
  lesson_title.tr!('０-９Ａ-Ｚａ-ｚ0-9A-Za-z', "0-9A-Za-z0-9A-Za-z")
  lesson_title.gsub!(corner, "") if corner
  lesson_title.gsub!(program_title_orig, "") if program_title_orig
  lesson_title = lesson_title.strip.sub(/　$/, "")
  lesson_title = lesson_title.sub(/番組を聴く$|\d{4}年\d{1,2}月\d{1,2}日(?:放送|\([月火水木金土日]\))$|\d{1,2}月\d{1,2}日[（(][月火水木金土日][）)]/, "")
  lesson_title.gsub(/[「」]/, "「" => "《", "」" => "》")
  lesson_title
end

### 番組IDからJSONへ変換
def program_parse(url_like_element_array)
  url_like_element_array.map.with_index(1) { |elem, i|
    if elem[ID_REGEX]
      ["https://www.nhk.or.jp/radioondemand/json/#{$2}/bangumi_#{$1}.json", 0]
    elsif elem[ID_REGEX_WITH_INDEX]
      ["https://www.nhk.or.jp/radioondemand/json/#{$2}/bangumi_#{$1}.json", $3.to_i]
    else
      nil
    end
  }.compact.transpose
end

### 番組の開始・終了時刻を Time オブジェクトで返す
def program_start_end_time(lesson)
  lesson_aa_vinfo4 = lesson['aa_vinfo4']
  if lesson_aa_vinfo4.nil?
    return [nil, nil]
  end
  begin
    start_time = Time.strptime(lesson_aa_vinfo4[/^[^_]+/], '%Y-%m-%dT%H:%M:%S%z')
    end_time = Time.strptime(lesson_aa_vinfo4[/[^_]+$/], '%Y-%m-%dT%H:%M:%S%z')
    return [start_time, end_time]
  ### aa_vinfo4: 9999-99-99T99:99:99+09:00_9999-99-99T99:99:99+09:00
  rescue ArgumentError
    return [nil, nil]
  end
end

def main(source_jsons, pickup_numbers)
  @is_win = is_win?
  unless check_ffmpeg_has_(/openssl|gnutls/)
    puts <<-EOB
gogakuondemand.rbを実行するには
OpenSSLまたはGnuTLSに対応しているFFmpegが必要です。
インストール済みのFFmpegを確認してください。
    EOB
    return
  end
  pickup_numbers_enum = pickup_numbers.to_enum
  source_objs = source_jsons.map.with_index(1) do |json, i|
    print "#{i} "
    JSON.parse(URI.open(json).read) rescue nil
  end
  print "\n"
  source_objs.each.with_index(1) do |source_obj, i|
    next if source_obj.nil?
    subject = source_obj['main']['program_name'].strip
    id = source_obj['main']['share_url'][%r[((P\d{6}|\d{4})_(?:\d\d))$]]
    puts "#{'%2d' % i}【#{subject}（#{id}）】のダウンロードを開始します。"
    update = false
    corner = source_obj['main']['corner_name']
    program_details = source_obj['main']['detail_list']
    pickup_number = pickup_numbers_enum.next

    program_details.each.with_index(1) do |program, i|
      next if pickup_number != 0 && i != pickup_number
      program_title_orig = program['headline']
      program_title = case program_title_orig
      when nil
        nil
      when /^\d{4}年\d{1,2}月\d{1,2}日\([月火水木金土日]\)$/
        nil
      when /^[「『][^」』]+[」』]/
        program_title_orig.strip
      else
        '《' + program_title_orig.strip + '》'
      end
      program_sub = program['headline_sub'] || ""
      program_sub = program_sub.strip.gsub(/<br>/i, "").gsub("\r\n", "…")

      program['file_list'].each do |lesson|
        file_id = lesson['file_id']
        lesson_title = fetch_title(lesson, corner, program_title_orig)
        lesson_title_sub = lesson['file_title_sub'] || ""
        program_start_time, program_end_time = program_start_end_time(lesson)
        if (program_start_time.nil? | program_end_time.nil?)
          lesson_onair_date = lesson['onair_date']
          next if lesson_onair_date.nil?
          date_mmdd = Date.strptime(lesson_onair_date[/\d{1,2}月\d{1,2}日/], "%m月 %d日")
          @date = (date_mmdd < Date.today) ? date_mmdd : date_mmdd.prev_year
        else
          @date = program_start_time.to_date
        end

        @open_time = Time.parse(lesson['open_time']) #表示にする時刻
        @close_time = Time.parse(lesson['close_time']) #非表示にする時刻
        next if Time.now < @open_time || @close_time < Time.now

        master_m3u8 = lesson['file_name']

        file_dirname = if corner && !corner[CORNER_REGEX]
          subject + "　" + corner
        else
          subject
        end

        program_title = program_title.gsub(/[《》]/, "") if (program_title && lesson_title.empty?)

        file_basename = "#{program_title}#{lesson_title}".gsub("/", ":")
        if file_basename.empty?
          lesson_title = @date.strftime('%Y/%m/%d')
          file_basename_ = ''
        else
          file_basename_ = file_basename + '_'
        end
        file_dir = PATH + file_dirname
        file_dir.mkpath unless file_dir.directory?
        file_path = file_dir + "#{file_basename_}#{@date.strftime('%Y_%m_%d')}.#{@output_ext}"
        next if file_path.file?
        FileUtils.touch(file_dir.parent.to_path)
        update = true
        puts file_path.basename
        command_ffmpeg = %Q[ffmpeg -y -http_seekable 0 -i "#{master_m3u8}" #{@output_options} -metadata album="#{file_dirname}#{program_title}" -metadata title="#{lesson_title}" -metadata artist="NHK" -metadata album_artist="#{program_sub + lesson_title_sub}" -metadata date="#{record_year}" -timeout 60 "#{file_path}" 2>&1]
        download(command_ffmpeg)

        ### ファイル更新日を放送開始時刻に設定
        file_path.utime(program_start_time, program_start_time) if file_path.file?
      end
    end
    puts(update ? "" : "更新はありません。")
  end
  puts "作業終了"
end

params = ARGV.getopts('', 'mp3', "path:#{Pathname.pwd}")
@output_ext, @output_options = params['mp3'] ? ['mp3', MP3] : ['m4a', M4A]
PATH = Pathname.new(params['path'])

source_jsons, pickup_numbers = program_parse(ARGV)
main(source_jsons, pickup_numbers)
