#!/usr/bin/env ruby
# coding: utf-8
# frozen_string_literal: true

require 'open-uri'
require 'optparse'
require 'pathname'
require 'fileutils'
require 'kconv'
require 'json'
require 'time'
require 'date'
$stdout.sync = true

WEEK_OF_THE_DAYS = %w[SU MO TU WE TH FR SA]
CORNER_REGEX = /[聴き]く$|NHKのど自慢/
ID_REGEX            = %r[(([A-Z0-9]{10}|\d{4}|P\d{6}|F\d{3})_(\d\d))$]
ID_REGEX_WITH_INDEX = %r[(([A-Z0-9]{10}|\d{4}|P\d{6}|F\d{3})_(\d\d))[^\d]+(\d+)$]
M4A = "-absf aac_adtstoasc -acodec copy"
MP3 = "-ab 64k -write_xing 0"

def check_ffmpeg_has_(regex)
  status = IO.popen('ffmpeg -version 2>&1') do |pipe|
    pipe.read
  end
  status[regex]
end

def is_win?
  RUBY_PLATFORM.downcase[/mswin(?!ce)|mingw|cygwin|bccwin/] ? true : false
end

def record_year
  @date.year
end

def time_counter_to_sec(counter)
  ((counter[0].to_i * 360000 + counter[1].to_i * 6000 + counter[2].to_i * 100 + counter[3].to_i) / 100.0).round
end

def progress_bar(progress, max, unit)
  max_digits = max.to_s.size
  print "\r" + "[#{'%-50s' % ('#' * (progress.to_f / max * 50).to_i)}] #{'%*d' % [max_digits, progress]} / #{max} #{unit}"
end

### ダウンロード進行状況の表示
def download(command_ffmpeg)
  duration = nil
  progress = 0
  counter_str = '(\d{2}):(\d{2}):(\d{2}).(\d{2})'
  line_buffer = ""
  received_error_message = false
  accept_to_quit = false
  cannot_start_error = false

  IO.popen(command_ffmpeg, "r+") do |pipe|

    pipe.each("r") do |line_raw|
      line = @is_win ? line_raw.tosjis : line_raw.toutf8

      ### ffmpeg が入力受付できる状況になった
      unless line.scan("[q]").empty?
        accept_to_quit = true
      end

      eachline_array = line.split(/\n|\r/)
      eachline_array_size = eachline_array.size

      eachline_array.each_with_index do |l, i|
        ln = ""
        if i == 0
          ln = line_buffer + l
        else
          ln = l
        end

        if eachline_array_size == i + 1
          if eachline_array_size == 1
            line_buffer = line_buffer + l
            next
          else
            line_buffer = l
          end
        end

        error_message_array = ln.scan(/^\[hls @ [^\]]+\] parse_playlist error|^\[crypto @ [^\]]+\] Unable to open resource/)
        unless error_message_array.empty?
          unless received_error_message
            case error_message_array.first
            when /parse_playlist/
              message = "　ダウンロードを開始出来ませんでした。"
              ### 入力受付できない状態でffmpegが終了するためメソッドを終了
              cannot_start_error = true
            when /Unable to open resource/
              message = "　ダウンロードファイルの一部を取得出来ませんでした。\n　ファイルが作成されているばあいには不完全ですので削除してください。"
            end
            puts " ！！！\n#{message}\n　スクリプトを再実行してください。"
            return if cannot_start_error
            received_error_message = true
          end
        end
      end

      ### エラー発生＆ffmpegに"q"を送って終了する
      if received_error_message && accept_to_quit
        pipe.puts "q"
        pipe.close_write
        return
      end

      counter_duration_match = line.scan(/: #{counter_str},/).flatten
      unless counter_duration_match == []
        duration = time_counter_to_sec(counter_duration_match)
      end
      counter_progress_match = line.scan(/time=#{counter_str}/).flatten
      unless counter_progress_match == []
        progress = time_counter_to_sec(counter_progress_match)
        progress = duration if duration && progress > duration
      end
      if duration && !received_error_message
        progress_bar(progress, duration, "秒")
      end
    end
    puts "完了"
  end
end

### 番組IDからJSONへ変換
def program_parse(url_like_element_array)
  url_like_element_array.map.with_index(1) { |elem, i|
    if elem[ID_REGEX]
      ["https://www.nhk.or.jp/radio-api/app/v1/web/ondemand/series?site_id=#{$2}&corner_site_id=#{$3}", $1, 0]
    elsif elem[ID_REGEX_WITH_INDEX]
      ["https://www.nhk.or.jp/radio-api/app/v1/web/ondemand/series?site_id=#{$2}&corner_site_id=#{$3}", $1, $4.to_i]
    else
      nil
    end
  }.compact.transpose
end

### 番組の開始・終了時刻を Time オブジェクトで返す
def program_start_end_time(aa_vinfo4)
  begin
    start_time = Time.strptime(aa_vinfo4[/^[^_]+/], '%Y-%m-%dT%H:%M:%S%z')
    end_time = Time.strptime(aa_vinfo4[/[^_]+$/], '%Y-%m-%dT%H:%M:%S%z')
    return [start_time, end_time]
  ### aa_vinfo4: 9999-99-99T99:99:99+09:00_9999-99-99T99:99:99+09:00
  rescue ArgumentError
    return [nil, nil]
  end
end

### 各番組でのタイトル及びサブタイトルの全角文字を半角に変換
def zenhan(str)
  if @is_win
    str.tr('０-９Ａ-Ｚａ-ｚ0-9A-Za-z', "0-9A-Za-z0-9A-Za-z")
  else
    str.tr('０-９Ａ-Ｚａ-ｚ！？，0-9A-Za-z　－', "0-9A-Za-z!?,0-9A-Za-z -").gsub(/[“”]/, '"')
  end
end

def main(source_jsons, site_ids, pickup_numbers)
  @is_win = is_win?
  unless check_ffmpeg_has_(/openssl|gnutls/)
    puts <<-EOB
gogakuondemand.rbを実行するには
OpenSSLまたはGnuTLSに対応しているFFmpegが必要です。
インストール済みのFFmpegを確認してください。
    EOB
    return
  end
  pickup_numbers_enum = pickup_numbers.to_enum
  source_objs = source_jsons.map.with_index(1) do |json, i|
    print "#{i} "
    JSON.parse(URI.open(json).read) rescue nil
  end
  print "\n"
  [source_objs, site_ids].transpose.each.with_index(1) do |(source_obj, site_id), i|
    if source_obj.empty?
      puts "#{'%2d' % i}\n ！！#{site_id} は存在しません。\n\n"
      next
    elsif source_obj.nil?
      puts "#{'%2d' % i}\n ！！#{site_id} のデータが読み取れません。\n\n"
      next
    end
    title = source_obj['title'].strip
    puts "#{'%2d' % i}【#{title}（#{site_id}）】のダウンロードを開始します。"
    update = false
    corner = source_obj['corner_name'].empty? ? nil : source_obj['corner_name']
    episodes = source_obj['episodes']
    pickup_number = pickup_numbers_enum.next

    episodes.each.with_index(1) do |program, i|
      next if pickup_number != 0 && i != pickup_number

#      program_id = program['id']
      program_title = zenhan(program['program_title'])
      master_m3u8 = program['stream_url']
      aa_contents_id = program['aa_contents_id']
      program_sub_title = zenhan(program['program_sub_title'])

      aa_measurement_id, aa_vinfo1, aa_vinfo2, aa_vinfo3, aa_vinfo4 = aa_contents_id.split(';')
      program_start_time, program_end_time = program_start_end_time(aa_vinfo4)
      @date = program_start_time.to_date

      file_dirname = corner && !corner[CORNER_REGEX] ? "#{title}　#{corner}" : title

      file_dir = PATH + file_dirname
      file_dir.mkpath unless file_dir.directory?
      file_path = file_dir + "#{program_title}_#{@date.strftime('%Y_%m_%d')}.#{@output_ext}"
      next if file_path.file?
      FileUtils.touch(file_dir.parent.to_path)

      update = true
      puts file_path.basename
      command_ffmpeg = %Q[ffmpeg -y #{@http_seekable} -i '#{master_m3u8}' #{@output_options} -metadata album='#{file_dirname}' -metadata title='#{program_title}' -metadata artist='NHK' -metadata album_artist='#{program_sub_title}' -metadata date='#{record_year}' -timeout 60 '#{file_path}' 2>&1]
      download(command_ffmpeg)

      ### ファイル更新日を放送開始時刻に設定
      file_path.utime(program_start_time, program_start_time) if file_path.file?
    end
    puts(update ? "" : "更新はありません。")
  end
  puts "作業終了"
end

params = ARGV.getopts("", "mp3", "path:#{Pathname.pwd}", "prior43")
@output_ext, @output_options = params["mp3"] ? ["mp3", MP3] : ["m4a", M4A]
PATH = Pathname.new(params['path'])
@http_seekable = (params["prior43"] || (PATH + "ffmpeg_prior_43.txt").exist?) ? "" : "-http_seekable 0"

source_jsons, site_ids, pickup_numbers = program_parse(ARGV)
main(source_jsons, site_ids, pickup_numbers)
