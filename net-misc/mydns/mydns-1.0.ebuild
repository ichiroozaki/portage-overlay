# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

DESCRIPTION="Set IP address into mydns server."

SLOT="0"
KEYWORDS="amd64 x86"

RDEPEND="net-misc/wget"

src_unpack() {
    mkdir "${S}"
}

src_install() {
    insinto /etc/cron.daily
    doins ${FILESDIR}/mydns
    fperms 0755 /etc/cron.daily/mydns
    insinto /etc
    doins ${FILESDIR}/mydns.conf
    fperms 0440 /etc/mydns.conf
}
