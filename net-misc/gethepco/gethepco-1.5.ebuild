# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

DESCRIPTION="Retrieve electricity demand data of Hokkaido Electric Co."
SRC_URI="https://bitbucket.org/ichiroozaki/gethepco/get/v${PV}.tar.gz"
MY_P="ichiroozaki-gethepco-e7b5e2081f22"

S=${WORKDIR}/${MY_P}       
SLOT="0"
KEYWORDS="amd64 x86 arm"

RDEPEND="dev-lang/ruby
        dev-ruby/mysql2"

src_install() {
    into /usr
    dobin "${S}/gethepco.rb"
    doinitd ${FILESDIR}/gethepco
}
