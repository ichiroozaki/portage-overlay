# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

DESCRIPTION="Retrieve power market data information from JEPX."
SRC_URI="https://bitbucket.org/ichiroozaki/${PN}/get/${PV}.tar.gz -> ${P}.tar.gz"
MY_P="ichiroozaki-get_jepx_info-96eda898f7b9"

S=${WORKDIR}/${MY_P}

SLOT="0"
KEYWORDS="amd64 x86 arm"

RDEPEND="dev-lang/ruby
        dev-ruby/mysql2"

src_install() {
    into /usr
    dobin "${S}/get_jepx_info.rb"
}
