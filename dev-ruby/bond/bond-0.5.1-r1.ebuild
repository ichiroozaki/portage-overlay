# $Header$

EAPI=7
USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="Bond is on a mission to improve autocompletion in ruby, especially for irb/ripl."
HOMEPAGE="https://github.com/cldwalker/bond"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

each_ruby_configure() {
		${RUBY} -Cext/readline_line_buffer extconf.rb || die
}

each_ruby_compile() {
		emake -Cext/readline_line_buffer V=1
}
