# $Header$

EAPI=8
USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="Client for the LINE notify API"
HOMEPAGE="https://github.com/YuzuruS/line_notify"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86 arm"
IUSE="-doc"
