# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Renders and parses JSON/XML/YAML documents from and to Ruby objects"
HOMEPAGE="https://rubygems.org/gems/representable"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

ruby_add_rdepend "
		<dev-ruby/declarative-0.1.0
		>=dev-ruby/trailblazer-option-0.1.1 <=dev-ruby/trailblazer-option-0.2.0
		<dev-ruby/uber-0.2.0"
