# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="URI class for parsing data URIs"
HOMEPAGE="https://github.com/dball/data_uri"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
