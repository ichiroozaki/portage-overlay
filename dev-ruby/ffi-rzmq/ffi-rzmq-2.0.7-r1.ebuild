EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="This gem wraps the ZeroMQ networking library using the ruby FFI (foreign function interface)."
HOMEPAGE="http://github.com/chuckremes/ffi-rzmq"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND+="net-libs/czmq"

ruby_add_rdepend ">=dev-ruby/ffi-rzmq-core-1.0.7"
