# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Allows simple authorization for accessing Google APIs"
HOMEPAGE="https://rubygems.org/gems/googleauth/"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "
		>=dev-ruby/faraday-0.17.3 <dev-ruby/faraday-2
		>=dev-ruby/jwt-1.4 <dev-ruby/jwt-3
		>=dev-ruby/memoist-0.16 <dev-ruby/memoist-1
		>=dev-ruby/multi_json-1.11 <dev-ruby/multi_json-2
		>=dev-ruby/os-0.9 <dev-ruby/os-2
		>=dev-ruby/signet-0.16 <dev-ruby/signet-1"
