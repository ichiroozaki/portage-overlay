# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Code generator for legacy Google REST clients"

HOMEPAGE="https://github.com/google/google-api-ruby-client"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "
		>=dev-ruby/activesupport-5.0 <dev-ruby/activesupport-6.0
		>=dev-ruby/google-apis-core-0.7 <dev-ruby/google-apis-core-2.0
		>=dev-ruby/google-apis-discovery_v1-0.5 <dev-ruby/google-apis-discovery_v1-1.0
		>=dev-ruby/thor-0.20 <dev-ruby/thor-2.0"
