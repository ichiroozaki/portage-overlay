# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="Script for converting HTML and ERB files to slim."
HOMEPAGE="https://github.com/slim-template/html2slim"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "
		dev-ruby/hpricot"
