EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

# Don't install the binaries since they don't seem to be intended for
# general use and they have very generic names leading to collisions,
RUBY_FAKEGEM_BINWRAP=""

inherit ruby-fakegem

DESCRIPTION="Call Python functions from the Ruby language"
HOMEPAGE="https://github.com/mrkn/pycall.rb"

LICENSE="MIT"
SLOT="1"
KEYWORDS="amd64 ~x86 ~arm"

each_ruby_configure() {
		${RUBY} -Cext/pycall extconf.rb || die
}

each_ruby_compile() {
		emake -Cext/pycall V=1
		cp ext/pycall/pycall$(get_modname) lib/ || die
}
