# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="Tk interface module using tcltklib"
HOMEPAGE="https://github.com/ruby/tk"

LICENSE="|| ( Ruby-BSD BSD-2 )"
SLOT="0"
KEYWORDS="amd64 ~x86 ~arm"
IUSE="-doc"

each_ruby_configure() {
	${RUBY} -Cext/tk extconf.rb || die "extconf.rb failed"
	${RUBY} -Cext/tk/tkutil extconf.rb || die "extconf.rb failed"
}

each_ruby_compile() {
	emake -Cext/tk V=1
	emake -Cext/tk/tkutil V=1
	cp ext/tk/tcltklib$(get_modname) ext/tk/tkutil/tkutil$(get_modname) lib/ || die
}
