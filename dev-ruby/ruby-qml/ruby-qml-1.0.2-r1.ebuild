# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="none"

inherit multilib ruby-fakegem

DESCRIPTION="QML/Qt Quick wrapper for Ruby"
HOMEPAGE="https://github.com/seanchas116/ruby-qml"
SRC_URI="https://github.com/seanchas116/ruby-qml/archive/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

RDEPEND=">=dev-qt/libqmlbind-0.0.1"

PATCHES=( "${FILESDIR}"/ruby-qml.patch )

each_ruby_configure() {
	case ${CHOST} in
		x86_64*) qmake="/usr/lib64/qt5/bin/qmake" ;;
		i?86*) qmake="/usr/lib/qt5/bin/qmake" ;;
	esac

	${RUBY} -Cext/qml extconf.rb --with-qmake="${qmake}" || die
}

each_ruby_compile() {
	emake -Cext/qml V=1
	rm lib/qml/qml.rb
	cp ext/qml/qml$(get_modname) lib/qml/qml$(get_modname) || die
}
