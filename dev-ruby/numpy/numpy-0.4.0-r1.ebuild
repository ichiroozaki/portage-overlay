# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

# Don't install the binaries since they don't seem to be intended for
# general use and they have very generic names leading to collisions,
RUBY_FAKEGEM_BINWRAP=""

inherit ruby-fakegem

DESCRIPTION="Numpy wrapper for Ruby"
HOMEPAGE="https://github.com/mrkn/numpy.rb"

LICENSE="MIT"
SLOT="1"
KEYWORDS="amd64 ~x86 arm"

DEPEND="
	dev-python/numpy
"

ruby_add_rdepend "
		>=dev-ruby/pycall-1.2.0
"
