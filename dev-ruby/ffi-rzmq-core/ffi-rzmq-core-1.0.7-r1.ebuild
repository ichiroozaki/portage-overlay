EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="This gem provides only the FFI wrapper for the ZeroMQ (0mq) networking library."
HOMEPAGE="http://github.com/chuckremes/ffi-rzmq-core"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "dev-ruby/ffi"
