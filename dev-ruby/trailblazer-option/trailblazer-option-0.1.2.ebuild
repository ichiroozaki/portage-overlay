EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Wrap an option at compile-time and 'call' it at runtime, which allows to have the common '-> ()', ':method' or 'Callable' pattern used for most options."
HOMEPAGE="https://trailblazer.to/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""
