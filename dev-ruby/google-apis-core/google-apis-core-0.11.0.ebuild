# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Common utility and base classes for legacy Google REST clients"

HOMEPAGE="https://github.com/google/google-api-ruby-client"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

ruby_add_rdepend "
		>=dev-ruby/addressable-2.5.1 <dev-ruby/addressable-3
		>=dev-ruby/googleauth-0.16.2 <dev-ruby/googleauth-2
		>=dev-ruby/httpclient-2.8.1 <dev-ruby/httpclient-3
		>=dev-ruby/mini_mime-1.0 <dev-ruby/mini_mime-1.1
		>=dev-ruby/representable-3.0 <dev-ruby/representable-4.0
		>=dev-ruby/retriable-2.0 <dev-ruby/retriable-4.0
		dev-ruby/rexml
		dev-ruby/webrick"
