EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="A Ruby kernel for Jupyter/IPython frontends (e.g. notebook)."
HOMEPAGE="https://github.com/SciRuby/iruby"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

PATCHES=(
	"${FILESDIR}/ext.patch"
)
ruby_add_rdepend ">=dev-ruby/mimemagic-0.3
				 >=dev-ruby/bond-0.5
				 >=dev-ruby/multi_json-1.11.0
				 >=dev-ruby/data_uri-0.1.0 <dev-ruby/data_uri-1
				 dev-ruby/ffi-rzmq"
