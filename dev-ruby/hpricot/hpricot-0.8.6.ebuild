# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="A swift, liberal HTML parser with a fantastic library."
HOMEPAGE="http://code.whytheluckystiff.net/hpricot/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
