# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Focused tests for Minitest"
HOMEPAGE="https://github.com/judofyr/minitest-line"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "=dev-ruby/minitest-5*"
