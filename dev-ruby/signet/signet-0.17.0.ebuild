# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="An OAuth 1.0 / OAuth 2.0 implementation."
HOMEPAGE="https://rubygems.org/gems/signet/"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

ruby_add_rdepend "
		>=dev-ruby/addressable-2.8 <dev-ruby/addressable-3
		>=dev-ruby/faraday-0.17.5 <dev-ruby/faraday-2
		>=dev-ruby/jwt-1.5 <dev-ruby/jwt-3
		>=dev-ruby/multi_json-1.10 <dev-ruby/multi_json-2"
