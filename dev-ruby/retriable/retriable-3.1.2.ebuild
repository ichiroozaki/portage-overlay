# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="none"

inherit ruby-fakegem

DESCRIPTION="An simple DSL to retry failed code blocks with randomized exponential backoff"
HOMEPAGE="https://rubygems.org/gems/retriable/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
