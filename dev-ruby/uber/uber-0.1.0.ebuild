# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_TEST="rake"
RUBY_FAKEGEM_RECIPE_DOC="none"

inherit ruby-fakegem

DESCRIPTION="A gem-authoring framework"
HOMEPAGE="https://rubygems.org/gems/uber/"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
