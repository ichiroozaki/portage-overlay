# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Id$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

RUBY_FAKEGEM_RECIPE_DOC="yard"

inherit ruby-fakegem

DESCRIPTION="Client for accessing Google APIs"
HOMEPAGE="https://github.com/google/google-api-ruby-client"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

ruby_add_rdepend "
		>=dev-ruby/google-apis-core-0.1 <dev-ruby/google-apis-core-1.0
		>=dev-ruby/google-apis-generator-0.1 <dev-ruby/google-apis-generator-1.0"
