# $Header$

EAPI=7
USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="rdoc"

RUBY_FAKEGEM_EXTRADOC="ChangeLog LICENSE README.md"

RUBY_FAKEGEM_RECIPE_TEST="rspec"

DESCRIPTION="The OS gem allows for some useful and easy functions, like OS.windows? (=> true or false) OS.bits ( => 32 or 64) etc"
HOMEPAGE="http://github.com/rdp/os"

SLOT="0"
KEYWORDS="amd64 ~x86"
