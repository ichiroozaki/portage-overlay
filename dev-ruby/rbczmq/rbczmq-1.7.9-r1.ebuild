# $Header$

EAPI=7

USE_RUBY="ruby30 ruby31 ruby32"

inherit ruby-fakegem

RUBY_FAKEGEM_RECIPE_DOC="none"

DESCRIPTION="ZeroMQ is a hybrid networking library / concurrency framework."
HOMEPAGE="https://github.com/methodmissing/rbczmq"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND+=" >=net-libs/czmq-3 <net-libs/czmq-4"

PATCHES=( "${FILESDIR}/rbczmq.patch" )

each_ruby_configure() {
	${RUBY} -Cext/rbczmq extconf.rb --with-system-libs|| die
}

each_ruby_compile() {
	emake -Cext/rbczmq V=1
	cp ext/rbczmq/rbczmq_ext$(get_modname) lib/ || die
}
