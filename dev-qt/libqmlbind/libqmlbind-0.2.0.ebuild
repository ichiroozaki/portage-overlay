EAPI=7

inherit qmake-utils multilib

DESCRIPTION="C library for creating QML bindings for other languages easily through exporting objects and classes to QML."

HOMEPAGE="https://github.com/seanchas116/libqmlbind"

SRC_URI="https://github.com/seanchas116/libqmlbind/archive/v${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86"
IUSE=""

DEPEND="dev-qt/qtcore:5"

RDEPEND="${DEPEND}"

src_configure() {
    eqmake5 -r
}

src_compile() {
    emake sub-qmlbind
}

src_install() {
    dolib.so qmlbind/libqmlbind$(get_modname)
    dolib.so qmlbind/libqmlbind$(get_libname 0)
    dolib.so qmlbind/libqmlbind$(get_libname 0.2)
    dolib.so qmlbind/libqmlbind$(get_libname 0.2.0)

    doheader -r qmlbind/include/*
}
