# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=8

inherit desktop java-pkg-2 java-ant-2 gnome2-utils

DESCRIPTION="GUI board for go games"
HOMEPAGE="https://github.com/Remi-Coulom/gogui"
SRC_URI="https://github.com/Remi-Coulom/gogui/archive/refs/tags/v${PV}.zip -> ${P}.zip"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="x86 amd64"
IUSE="doc gnome"

RDEPEND=">=virtual/jre-1.8"

BDEPEND=">=virtual/jdk-1.8
		 app-text/docbook-xsl-stylesheets
		 dev-java/javatoolkit
		 media-gfx/inkscape"

src_prepare() {
	eapply -p0 "${FILESDIR}"/install.sh.patch
	eapply_user
}

src_compile()
{
	(cd src/net/sf/gogui/images/ && ./svg.sh) || die
	ant -Ddocbook-xsl.dir=/usr/share/sgml/docbook/xsl-stylesheets  -Dquaqua.ignore=true
}

src_install()
{
	./install.sh -p "${D}/usr" -s "${D}/etc"

	doicon -s 48x48 src/net/sf/gogui/images/gogui-48x48.png
	doicon -s 128x128 src/net/sf/gogui/images/gogui-128x128.png
	doicon -s scalable src/net/sf/gogui/images/gogui.svg
	doicon -s 48x48 -c mymetypes config/application-x-go-sgf.png
	domenu config/gogui.desktop
	use doc && dodoc -r doc/.
	use gnome && gnome2_gconf_install --makefile-install-rule "${D}/etc/gconf/schemas/gogui.schemas"
}

pkg_postinst() {
	gnome2_icon_cache_update
	fdo-mime_mime_database_update
	fdo-mime_desktop_database_update
}

pkg_postrm() {
	gnome2_icon_cache_update
	fdo-mime_mime_database_update
	fdo-mime_desktop_database_update
}
