# Distributed under the terms of the GNU General Public License v2

# Using a binary ebuild until a source ebuild is doable.

EAPI=8

MY_P="${P/-bin}"
MY_PN="${PN/-bin}"

DESCRIPTION="An extensible repository management software"
HOMEPAGE="https://archiva.apache.org/index.cgi"
SRC_URI="http://ftp.kddilabs.jp/infosystems/apache/archiva/${PV}/binaries/${MY_PN}-${PV}-bin.tar.gz"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86"

S="${WORKDIR}/${MY_P}"

RDEPEND=">=virtual/jre-1.8"
DEPEND=">=virtual/jdk-1.8"

PATCHES=(
		"${FILESDIR}/archiva.patch"
)

ARCHIVA_VAR="/var/archiva"

src_install() {
	local installdir="/opt/apache-archiva"

	dodoc LICENSE NOTICE

	insinto "${installdir}"
	doins -r apps conf contexts logs temp

	exeinto "${installdir}/bin"
	doexe bin/archiva
	if use amd64; then
		doexe bin/wrapper-linux-x86-64
	elif use x86; then
		doexe bin/wrapper-linux-x86-32
	fi

	exeinto "${installdir}/lib"
	doexe lib/*.jar
	if use amd64; then
		doexe lib/libwrapper-linux-x86-64.so
	elif use x86; then
		doexe lib/liwrapper-linux-x86-32.so
	fi

	echo "CONFIG_PROTECT=\"${ARCHIVA_VAR}/conf\"" > "${T}/30apache-archiva"
	doenvd "${T}/30apache-archiva"

	insinto "${ARCHIVA_VAR}/conf"
	doins conf/*

	doinitd "${FILESDIR}/apache-archiva"

	keepdir "${ARCHIVA_VAR}/data"
	keepdir "${ARCHIVA_VAR}/logs"
	keepdir "${ARCHIVA_VAR}/temp"
}
