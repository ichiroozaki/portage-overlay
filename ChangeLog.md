# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/).


## 2025-01-14
### Added
- Add net-misc/radirudegogaku-1.1.0.ebuild

## 2024-11-18
### Added
- Add torchaudio/torchaudio-2.5.1.ebuild
- Add torchvision/torchvision-0.20.1.ebuild

## 2024-11-04
### Changed
- Change PYTHON_COMPAT to 10..12 in all dev-python ebuilds

## 2024-10-15
### Added
- Add games-bard/gogui-1.4.9-r2.ebuild
- Add games-bard/gogui-1.5.4a.ebuild

## 2024-09-16
### Added
- Add torchaudio/torchaudio-2.4.1.ebuild
- Add torchvision/torchvision-0.19.1.ebuild

### Rmoved
- Remove app-i18n/mozc-2.28.5029.102-r2.ebuild

## 2024-08-29
### Added
- Add app-i18n/mozc-2.28.5029.102-r2.ebuild

## 2024-08-12
### Added
- Added dev-python/torchaudio-2.4.0.ebuild
- Added dev-python/torchvision-0.19.0.ebuild

## 2024-07-19
### Added
- Added dev-python/torchaudio-2.3.1.ebuild
- Added dev-python/torchvision-0.18.1.ebuild

## 2024-05-04
### Added
- Added dev-python/torchaudio-2.3.0.ebuild
- Added dev-python/torchvision-0.18.0.ebuild

## 2024-04-09
### Added
- Added dev-python/xformers-0.0.25.ebuild
- Added dev-python/auto-gptq-0.7.1.ebuild
- Added dev-python/accelerate-0.29.1.ebuild

## 2024-04-06
### Added
- Added dev-python/torchaudio-2.2.2.ebuild
- Added dev-python/torchvision-0.17.2.ebuild

## 2024-03-23
### Added
- Added dev-python/pymcuprog-3.17.3.45.ebuild
- Added dev-python/pyedbglib-2.24.2.18.ebuild

## 2024-03-22
### Added
- Added dev-python/bitsandbytes-0.42.0.ebuild

## 2024-03-03
### Added
- Added dev-ruby/faraday-cookie_jar-0.0.7.ebuild

## 2024-01-27
### Added
- Added dev-python/torchvision-0.16.2.ebuild
- Added dev-python/torchaudio-2.1.2.ebuild
- Added dev-python/accelerate-0.25.0.ebuild

## 2023-12-02
### Added
- Added dev-python/bitsandbytes-0.41.2.ebuild

##  2023-11-27
### Added
- Added dev-python/gekko-1.0.6.ebuild
- Added dev-python/auto-gptq-0.5.1.ebuid
- Added dev-python/peft-0.6.2.ebuild

## 2023-11-03
### Added
- Added dev-python/flashlight-text-0.0.4.ebuild

## 2023-09-28
### Added
- Added dev-python/accelerate-0.23.0.ebuild
- Added dev-python/diffusers-0.21.3.ebuild

## 2023-09-13
### Added
- Added dev-python/xformers-0.0.21.ebuild
- Added dev-python/sentencepiece-0.1.99.ebuid

## 2023-09-05
### Added
- Added dev-python/fugashi-1.3.0.ebuild
- Added dev-python/unidic-lite-1.0.8.ebuild

## 2023-09-03
### Added
- Added dev-util/apache-archiva-bin-2.2.10

### Removed
- Removed dev-util/apache-archiva-bin-2.2.5
- Removed dev-util/apache-archiva-bin-2.2.3

## 2023-08-13
### Added
- Add dev-ruby/sinatra-contrib-3.0.6.ebuild

## 2023-08-11
### Removed
- Remove dev-ruby/ruby-mqtt

## 2023-08-07
### Removed
- Remove app-i18n/mozc

## 2023-07-20
### Removed
- Remove app-misc/boostnote-bin
- Remove app-misc/nixnot2

## 2023-07-18
### Added
- Add net-misc/radirudegogaku-1.0.0.ebuild

### Removed
- Remove net-misc/capturestream

## 2023-07-15
### Added
- Add dev-ruby/line_notify-1.0.1.ebuild

### Fixed
- Rename jupyterlab_git to jupyterlab-git

## 2023-07-06
### Fixed
- Fix nbstripout-0.6.1.ebuild

## 2023-05-19
### Changed
- Change pytest-runner/pytest-runner-5.3.2.ebuild
- Change USE_RUBY varibles

## 2023-05-07
### Changed
- Change dev-python/jupyterlab_git-0.41.0.ebuild to support python 3.11.x
- Change pytest-runner/pytest-runner-5.3.2.ebuild to support python 3.11.x

### Removed
- Remove dev-python/jupyterlab_git-0.30.0.ebuild
- Remove dev-python/jupyterlab_git-0.37.1.ebuild
- Remove nbstripout/nbstripout-0.3.9.ebuild
- Remove nbstripout/nbstripout-0.4.0.ebuild
- Remove nbstripout/nbstripout-0.5.0.ebuild
- Remove pytest-runner/pytest-runner-5.3.0.ebuild
- Remove dev-python/json5
- Remove dev-python/smart_open

## 2023-04-14
### Added
- Add dev-ruby/sinatra-contrib-3.0.5
- Add dev-ruby/activerecord-import-1.4.1.ebuild
- Add dev-ruby/ruby-mqtt-0.6.0.ebuild

### Removed
- Remove dev-ruby/activerecord-import-0.25.0.ebuild
- Remove dev-ruby/activerecord-import-1.0.3.ebuild
- Remove dev-ruby/iruby-0.2.9.ebuild
- Remove dev-ruby/iruby-0.4.0.ebuild
- Remove dev-ruby/ruby-mqtt-0.3.1.ebuild
- Remove dev-ruby/ruby-mqtt-0.4.0.ebuild
- Remove dev-ruby/mocha-on-bacon-0.2.2.ebuild
- Remove dev-ruby/chartkick-2.1.1.ebuild
- Remove dev-ruby/chartkick-2.3.4.ebuild
- Remove dev-ruby/chartkick-3.3.1.ebuild
- Remove dev-ruby/chartkick-3.4.2.ebuild

### Fixed
- Fix dev-ruby/mocha-on-bacon-0.2.3.ebuild

## 2023-04-13
### Added
- Add dev-ruby/sinatra-activerecord-2.0.26

## 2023-03-19
### Added
- Add jupyterlab_git-0.41.0

## 2023-01-02
### Added
- Add dev-ruby/sinatra-contrib-2.2.3

### Removed
- Remove dev-ruby/sinatra-contrib-2.0.8.1.ebuild
- Remove dev-ruby/sinatra-contrib-2.1.0.ebuild

## 2022-11-02
### Removed
- Remove dev-python/tomlkit

## 2022-07-12
### Fixed
- Fix app-i18n/mozc-2.26.4632.100_p20220130102938_p20220202184242
- Fix dev-ruby/ruby-mqtt-0.5.0

## 2022-07-03
### Added
- Add dev-ruby/pycall-1.4.1
- Add dev-python/jupyter_git-0.37.1
- Add dev-python/nbstripout-0.5.0
- Add dev-python/pytest-runner-5.3.2

## 2022-07-02
### Fixed
- Fix dev-ruby/sinatra-contrib-2.2.0

## 2022-06-26
### Added
- Add dev-ruby/sinatra-contrib-2.2.0

## 2022-06-20
### Added
- Add mocha-on-bacon-0.2.3

### Fixed
- Fix app-i18n/mozc-2.26.4632.100_p20220130102938_p20220202184242
- Fix dev-ruby/bond-0.5.1
- Fix dev-ruby/data_uri-0.1.0
- Fix dev-ruby/ffi-rzmq-1.0.7
- Fix dev-ruby/ffi-rzmq-core-1.0.7
- Fix dev-ruby/bacon-bits-0.1.0

## 2022-06-19
### Added
- Add dev-ruby/sinatra-activerecord-2.0.25
- Add dev-ruby/activerecord-import-1.4.0
- Add dev-ruby/chartkick-4.2.0
- Add dev-ruby/iruby-0.7.4
- Add dev-ruby/numpy-0.4.0
- Add dev-ruby/pandas-0.3.8

### Fixed
- Fix dev-ruby/rbczmq-1.7.9

## 2022-02-26
- Added app-i18n/mozc

## 2022-02-15
- Removed dev-python/jupyter-packaging, jupyter_server, jupyterlab, jupyterlab_server, nbdime, nbclassic, and anyio

## 2021-08-15
- Add dev-perl/HTML-TagCloud-0.38-r1.ebuild

## 2020-06-11
- Change python interpreter dependency to python3_{7,8,9} in dev-python/jupyterlab-3.0.14.ebuild

## 2020-06-05
- Removed net-misc/onedrive-2.4.12.ebuild

## 2020-06-05
- Added net-misc/onedrive-2.4.12.ebuild

## 2020-05-15
- Change python interpreter dependency to python3_{7,8,9} in all python packages

## 2020-05-01
- Added dev-python/nbstripout-0.4.0.ebuild

## 2020-04-30
- Added dev-python/tomlkit-0.7.0.ebuild
- Added dev-python/jupyterlab_server-2.5.0.ebuild
- Added dev-python/jupyter-packaging-0.9.2.ebuild
- Added dev-python/jupyter_server-1.6.4.ebuild
- Added dev-python/jupyterlab-3.0.14.ebuild
- Added dev-python/jupyterlab_git-0.30.0.ebuild

## 2020-02-14
- Added dev-ruby/ffi-rzmq-2.0.7
- Added dev-ruby/ffi-rzmq-core-1.0.7

## 2020-02-13
- Added dev-python/anyio
- Added dev-python/jupyter-packaging
- Added dev-python/jupyter_server
- Added dev-python/nbclassic
- Added dev-python/jupyterlab-3.0.7
- Added dev-python/nbstripout-0.3.9
- Added dev-python/pytest-runner-5.3.0
- Added dev-python/jupyterlab_git-0.23.3
- Added dev-python/jupyterlab_git-0.30.0_beta
- Added dev-python/nbdime-3.0.0_beta
- Added dev-util/apache-archiva-bin-2.2.5

### Removed
- Removed dev-embedded/arduino
- Removed dev-ml/ocamlnet
- Removed dev-ruby/rubocop
- Removed media-libs/libldac
- Removed media-video/ffmpeg
- Removed dev-util/apache-archiva-bin-2.2.1

## 2020-02-11
### Removed
- Removed dev-ruby/activesupport

## 2021-01-27
### Added
- Added dev-ruby/sinatra-activerecord-2.0.22.ebuild

## 2021-01-26
### Added
- Added dev-ruby/chartkick-3.4.2.ebuild

## 2021-01-25
### Added
- Added dev-ruby/google-api-client-0.53.0.ebuild
- Added dev-ruby/google-apis-discovery_v1-0.1.0.ebuild
- Added dev-ruby/google-apis-generator-0.1.2.ebuild
- Added dev-ruby/google-apis-core-0.2.1.ebuild
- Added dev-ruby/googleauth-0.14.0.ebuild
- Added dev-ruby/signet-0.14.0.ebuild

### Removed
- Removed dev-ruby/google-api-client-0.37.2.ebuild
- Removed dev-ruby/google-api-client-0.40.2.ebuild

## 2020-12-30
### Fixed
- Fix license in dev-qt/libqmlbind-0.2.0.ebuild
- Fix dependency on dev-qt/libqmlbind in dev-ruby/ruby-qml-1.0.2.ebuild

## 2020-12-28
### Fixed
- Fix EAPI in games-board/gogui-1.4.9 and replaced to 1.4.9-r1

## 2020-11-15
### Removed
- Removed sec-policy

## 2020-09-26
### Added
- Added dev-ruby/google-api-client-0.45.0.ebuild

### Removed
- Removed dev-ruby/google-api-client-0.27.1.ebuild
- Removed dev-ruby/google-api-client-0.19.4.ebuild

## 2020-09-21
### Added
- Add sinatra-contrib-2.1.0.ebuild

### Removed
- Removed sinatra-contrib-1.4.7.ebuild
- Removed sinatra-contrib-2.0.5.ebuild
- Removed sinatra-contrib-2.0.7.ebuild

## 2020-08-11
### Added
- Add dev-ruby/pandas-0.3.2

## 2020-07-31
### Fixed
- Fix dependency on czmq-3 in dev-ruby/rbczmq-1.7.9.ebuild

## 2020-07-31
### Added
- Add app-misc/boost-note-bin-0.8.2

### Removed
- Removed dev-util/android-studio-4.0.1.0.193.6626763.ebuild

## 2020-07-26
### Added
- Add dev-ruby/rubocop-0.88.0-r1.ebuid
- Add app-misc/boostnote-0.16.0.ebuild

### Removed
- Remove <app-misc/boostnote-0.13

## 2020-07-23
### Added
- Add dev-util/android-studio-4.0.1.0.193.6626763.ebuild

## 2020-07-20
### Added
- Add dev-ruby/evernote-thrift-1.25.2.ebuild and dev-ruby/evernote_oauth-0.2.3.ebuild

## 2020-07-07
### Added
- Add app-misc/boost-note-bin-0.7.0

## 2020-06-16
### Added
- Add dev-ruby/google-api-client-0.40.2.ebuild

## 2020-06-07
### Added
- Add app-misc/boost-note-bin-0.6.1

## 2020-06-03
### Added
- Add app-misc/boost-note-bin-0.5.0

## 2020-05-28
### Changed
- Add app-misc/nixnote2-2.1.6.ebuild

## 2020-05-16
### Changed
- Change KEYWORD="arm" in dev-ruby/chartkick

## 2020-05-10
### Added
- Add dev-python/jupyterlab-2.1.0
- Add dev-python/jupyterlab_server-1.1.1
- Add dev-python/json5-0.9.4.ebuild

## 2020-05-06
### Added
- Add dev-embedded/arduino-1.8.12

## 2020-05-04
### Added
- Add media-libs/libldac-2.0.2.3
- Add app-misc/boostnote-bin-0.15.3
- Add app-misc/boost-note-bin-0.4.1

## 2020-04-11
### Added
- Add app-misc/boostnote-bin-0.15.2

## 2020-03-21
### Added
- Add dev-ruby/sinatra-activerecord-2.0.14

## 2020-03-14
### Added
- Add app-misc/nixnote2-2.1.4.ebuild

### Fixed
- Fix dependency in dev-ruby/sinatra-2.0.8.1
- Add filex in QA_PREBUILD in app-misc/boostnote-bin-0.15.1

## 2020-03-12
### Added
- Add dev-ruby/sinatra-2.0.8.1
- Add app-misc/boostnote-bin-0.15.1

## 2020-02-11
### Added
- Add dev-ruby/signet-0.12.0
- Add net-misc/grive-9999

## 2020-02-01
### Removed
- Removed dependency on dev-ruby/bones from dev-ruby/logging-2.2.2-r1

## 2020-01-24
### Added
- Add dev-ruby/pycall-1.3.0
- Add dev-ruby/numpy-0.2.0
- Add dev-ruby/pandas-0.3.1

## 2020-01-23
### Added
- Add dev-ruby/chartkick-3.3.1
- Add dev-ruby/sinatra-activerecord-2.0.13
- Add dev-ruby/activesupport--4.2.11.1-r1

### Fixed
- Change EAPI=7 in all packages
- Change USE_RUBY="ruby24 ruby25 ruby26" in all packages
- Change to SLOT="0" in dev-ruby/chartkick-3.3.1

### Removed
- Removed old packages

## 2019-12-04
## Fixed
- Change EAPI=7 app-misc/nixnote2-2.1.[234].ebuild

## 2019-11-24
### Added
- Add dev-ruby/activerecord-import-1.0.3.ebuild

## 2019-10-22
### Removed
- Removed old packages

## 2019-10-21
### Added
- Add app-misc/boostnote-bin-0.13.0.ebuild

## 2019-09-29
### Added
- Add app-misc/nixnote2-2.1.4.ebuild

## 2019-09-09
### Added
- Add dev-ruby/sinatra-contrib-2.0.7.ebuild

## 2019-08-23
### Added
- Add dev-ruby/actionpack-xml\_parser-2.0.1-r1.ebuild
- Add dev-ruby/csv-3.1.1.ebuild
- Add dev-ruby/benchmark\_driver-0.14.21.ebuild

## 2019-08-21
### Added
- Add dev-ruby/bones-3.8.4-r3.ebuild

## 2019-07-31
### Added
- Add app-misc/boostnote-bin-0.12.1.ebuild

## 2019-07-20
### Fixed
- Changed SLOT in dev-ruby/sinatra-contrib

## 2019-05-27
### Added
- Add app-misc/boostnote-bin-0.11.17.ebuild

## 2019-05-27
### Fixed
- Fix file size in Manifest

## 2019-05-06
### Added
- Add media-video/ffmpeg-4.1.3-r1.ebuild with vidstab USE flag

## 2019-05-25
### Fixed
- Change LICENSE from "MIT License" to "MIT"

## 2019-05-03
### Added
- Add media-video/ffmpeg-3.4.5-r1.ebuild with vidstab USE flag

## 2019-04-30
### Added
- Add app-misc/nixnote2-2.1.3.ebuild

## 2019-04-15
### Added
- Add dev-ml/ocamlfuse-2.7.1-r2.ebuild

## 2019-04-15
### Added
- Add dev-ml/ocamlnet-4.1.6.ebuild

## 2019-04-15
### Added
- Add dev-libs/nettle-3.3-r2.ebuild

## 2019-03-02
### Added
- Add app-misc/nixnote2-2.1.2.ebuild

## 2019-02-09
### Added
- Add dev-ruby/sinatra-contrib-2.0.5

## 2019-01-27
### Added
- Add app-misc/boostnote-bin-0.11.13.ebuild

## 2019-01-19
### Added
- Add dev-ruby/tk-0.2.0.ebuild

### Fixed
- Fix bug in tk-0.2.0.ebuild

## 2018-12-24
### Added
- Add app-misc/boostnote-bin-0.11.12.ebuild

## 2018-11-23
### Added
- Add dev-ruby/logging-2.2.2.ebuild

## 2018-11-17
### Added
- Add dev-ruby/sinatra-contrib-2.0.4

## 2018-11-15
### Added
- Add net-misc/get\_jepx\_info-4.0.0.ebuild

## 2018-11-13
### Added
- Add dev-ruby/by_star-3.0.0.ebuild

## 2018-10-27
### Added
- Add dev-libs/cudnn-7.3.1.20.ebuild

## 2018-10-19
### Added
- Add app-misc/boostnote-bin-0.11.10.ebuild
- Add app-misc/nixnote2-2.0.2-r1.ebuild

### Changed
- Delete app-misc/boostnote
- Delete app-misc/nixnote

## 2018-09-22
### Added
- Add app-misc/boostnote-bin-0.11.9.ebuild

## 2018-09-01

### Changed
- Delete media-sound/lastfm-desktop

## 2018-08-14
### Added
- Add android-studio-3.1.4.0.173.4907809.ebuild
- Add app-misc/boostnote-bin-0.11.8.ebuild

## 2018-08-13
### Added
- Add cudnn-7.1.4-r1.ebuild

## 2018-07-30
### Added
- Add net-misc/get\_jepx\_info-3.0.1.ebuild

## 2018-07-27
### Added
- Add net-misc/get\_jepx\_info-3.0.0.ebuild

## 2018-07-15
### Added
- Add net-misc/get\_jepx\_info-2.0.0.ebuild

## 2018-07-15
### Added
- Add dev-ruby/activerecord-import-0.25.0.ebuild

## 2018-06-30
### Added
- Add app-misc/boostnote-bin-0.11.7.ebuild

## 2018-05-09
### Added
- Add app-misc/boostnote-bin-0.11.4.ebuild

## 2018-05-07
### Added
- Add net-misc/gethepco-1.5.ebuild

## 2018-05-01
### Added
- Add net-misc/gethepco-1.4.1.ebuild

## 2018-04-30
### Added
- Add dev-ruby/chartkick-2.3.4.ebuild

## 2018-04-29
### Added
- Add net-misc/gethepco-1.4.ebuild

## 2018-03-25
### Changed
- Change USE\_RUBY to "ruby22 ruby23" in dev-ruby/charkick, dev-ruby/faraday-cookie_jar and dev-ruby/iruby

## 2018-03-25
### Added
- Add app-misc/boostnote-bin-0.11.2

## 2018-02-10
### Fixed
- [dev-ruby/os-0.9.6] Fix RUBY\_FAKEGEM\_EXTRADOC

## 2018-01-27
### Added
- Add dev-ruby/bones-3.8.4.ebuild
- Add dev-ruby/googleauth-0.6.2.ebuild
- Add dev-ruby/os-0.9.6.ebuild
- Add dev-ruby/signet-0.8.1.ebuild

## 2018-01-23
### Added
- Add dev-ruby/declarative-0.0.10.ebuild
- Add dev-ruby/google-api-client-0.19.4.ebuild
- Add dev-ruby/retriable-3.1.1.ebuild

## 2018-01-03
### Fixed
- [dev-embedded/arduino-1.8.5] Add fperm +x to binary files

## 2017-12-17
### Added
- Add net-misc/mediatomb from gentoo main repository

## 2017-11-23
### Fixed
- [dev-embedded/arduino-1.8.5] Add pkg\_postinst() and pkg\_postrm()

## 2017-11-23
### Changed
- Change dev-embedded/arduino to dev-embedded/arduino-bin

## 2017-11-23
### Added
- Add dev-embedded/arduino-1.8.5
- Add app-misc/boostnote-bin-0.8.16

## 2017-10-17
### Fixed
- [dev-ruby/sinatra-contrib-1.4.7] Add RUBY\_FAKEGEM\_TASK_DOC and RUBY\_FAKEGEM\_DOCDIR

## 2017-10-07
### Fixed
- [app-misc/nixnote2-2.0.2] Change dependency on opencv3

## 2017-10-04
### Added
- Add app-misc/nixnote2-2.0.2

## 2017-09-28
### Added
- Add www-apps/gitweb-2.13.6

## 2017-09-18
### Added
- Add dev-util/apache-archiva-bin-2.2.3

## 2017-09-18
### Added
- Add sys-apps/lm\_sensors-3.4.0\_p20170326

## 2017-09-16
### Fixed
- Add arm KEYWORD to dev-ruby/sinatra-contrib-1.4.7

## 2017-09-16
### Added
- Add dev-ruby/sinatra-contrib-1.4.7

## 2017-08-27
### Fixed
- Change RUBY\_FAKEGEM\_RECIPE\_TEST to rake in dev-ruby/uber-0.0.15

## 2017-08-27
### Fixed
- Add amd64 keyword to dev-ruby/bones-3.8.3

## 2017-08-27
### Added
- Add dev-ruby/bones-3.8.3

## 2017-08-27
### Added
- Add dev-ruby/google-api-client-0.13.1
- Add dev-ruby/google-auth-0.5.0
- Add dev-ruby/minitest-line-0.6.0
- Add dev-ruby/representable-3.0.0
- Add dev-ruby/retriable-2.1.0
- Add dev-ruby/signet-0.7.0
- Add dev-ruby/uber-0.0.15

## 2017-08-27
### Removed
- Delete dev-ruby/google-api-client-0.9.6

## 2017-08-26
### Added
- Add media-sound/lastfm-desktop-2.1.36-r1

## 2017-08-25
### Added
- Add www-apps/gitweb-2.13.5

## 2017-08-12
### Fixed
- Remove depenency on googleauth in dev-ruby/google-api-client-0.9.6
- Change KEYWORDS in dev-ruby/google-api-client-0.9.6

## 2017-08-21
### Changed
- Change name of dev-ruby/google-api-ruby-client to dev-ruby/google-api-client
- Change dev-ruby/google-api-client to use gem file

## 2017-08-14 [9558619aab95a9f150a53a1b58d1e8ef84a55e1d]
### Added
- Add README.md

## 2017-08-11 [45cda574033971400b7bc24e675add2e3eb0a02d]
### Added
- Add dev-libs/cudnn-5.1.ebuild

## 2017-06-25 [8065a7e9fc108b7fdc3fc419e73ed5adec7cd0ce]
### Added
- Add dev-ruby/iruby-0.2.9.ebuild
- Add dev-ruby/data_uri-0.1.0.ebuild

## 2017-06-25 [20de99929955b0424eb125e30dc9eb8d956d36e8]
### Added
- Add dev-ruby/iruby-0.2.8.ebuild

## 2017-05-07 [3618e693502e63574a80de1b80656dc2599d9d00]
### Added
- Add dev-perl/HTML-Tagcloud-0.38

## 2017-05-04
### Added
- Add dev-util/apache-archiva-bin-2.2.1
