# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
PYPI_NO_NORMALIZE=true
DISTUTILS_EXT=1
DISTUTILS_SINGLE_IMPL=1
inherit distutils-r1 cuda

DESCRIPTION="An easy-to-use LLMs quantization package with user-friendly apis, based on GPTQ algorithm."
HOMEPAGE="https://github.com/PanQiWei/AutoGPTQ"
SRC_URI="https://github.com/PanQiWei/AutoGPTQ/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
MY_PN="AutoGPTQ"
MY_P="${MY_PN}-${PV}"
S="${WORKDIR}/${MY_P}"

LICENSE="MIT"
SLOT="0"

KEYWORDS="amd64"

RESTRICT="test"

DEPEND="dev-python/gekko
		>=dev-python/accelerate-0.26.0
		sci-libs/datasets
		dev-python/sentencepiece
		dev-python/numpy
		>=sci-libs/pytorch-1.13.0
		sci-libs/safetensors
		>=sci-libs/transformers-4.31.0
		>=dev-python/peft-0.5.0
		dev-python/tqdm
"

PATCHES=(
	"${FILESDIR}/exclude_test.patch"
	"${FILESDIR}/compiler.patch"
)

src_prepare() {
	default
}

python_compile() {
	export CC="$(cuda_gccdir)/gcc"
	export CXX="$(cuda_gccdir)/g++"
	export CUDAHOSTCXX="$(cuda_gccdir)/g++"

	distutils-r1_python_compile -j1
}
