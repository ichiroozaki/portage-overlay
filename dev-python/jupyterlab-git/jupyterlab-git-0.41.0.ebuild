# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_USE_SETUPTOOLS=bdepend

inherit distutils-r1

DESCRIPTION="A server extension for JupyterLab's git extension"
HOMEPAGE="https://github.com/jupyterlab/jupyterlab-git"
SRC_URI=https://github.com/jupyterlab/jupyterlab-git/archive/refs/tags/v${PV}.tar.gz

LICENSE="BSD"
SLOT="0"
KEYWORDS="amd64 ~hppa ~ia64 ~x86"
IUSE="test"
RESTRICT="!test? ( test )"

RDEPEND="
	>=dev-python/jupyter-server-1.21.0 <dev-python/jupyter-server-3
	>=dev-python/nbdime-3.1 <dev-python/nbdime-4
	dev-python/nbformat
	dev-python/packaging
	dev-python/pexpect
	>=dev-python/traitlets-5.0 <dev-python/traitlets-6
"
