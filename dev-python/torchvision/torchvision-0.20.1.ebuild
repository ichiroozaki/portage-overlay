# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_EXT=1
inherit distutils-r1 cuda

DESCRIPTION="image and video datasets and models for torch deep learning"
HOMEPAGE="https://github.com/pytorch/vision"
SRC_URI="https://github.com/pytorch/vision/archive/refs/tags/v${PV}.tar.gz
		-> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"

KEYWORDS="amd64 ~hppa ~ia64 ~x86"

RESTRICT="test"

RDEPEND="
		=sci-libs/pytorch-2.5*[${PYTHON_SINGLE_USEDEP}]
		>=media-video/ffmpeg-6
"

S="${WORKDIR}/vision-${PV}"

src_prepare() {
	default
}

python_compile() {
	addpredict "/proc/self/task"

	local -x MAKEOPTS=-j1

	export CC="$(cuda_gccdir)/gcc"
	export CXX="$(cuda_gccdir)/g++"
	export CUDAHOSTCXX="$(cuda_gccdir)/g++"
	export TORCH_INSTALL_PREFIX="$(python_get_sitedir)/torch"

	distutils-r1_python_compile
}
