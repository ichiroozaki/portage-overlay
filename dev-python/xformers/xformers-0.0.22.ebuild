# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_EXT=1
inherit python-single-r1 distutils-r1 pypi cuda

DESCRIPTION="A collection of composable Transformer building blocks"
HOMEPAGE="https://facebookresearch.github.io/xformers/"
SRC_URI="$(pypi_sdist_url)"

LICENSE="BSD"
SLOT="0"

KEYWORDS="amd64 ~hppa ~ia64 ~x86"

RESTRICT="test"

RDEPEND="
		sci-libs/pytorch[${PYTHON_SINGLE_USEDEP}]
"

PATCHES=(
	"${FILESDIR}/flash.patch"
)

src_prepare(){
	default
	export MAX_JOBS=${XFORMERS_MAX_JOBS:-2}
}
