# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_EXT=1
inherit python-r1 distutils-r1 pypi

DESCRIPTION="k-bit optimizers and matrix multiplication routines"
HOMEPAGE="https://github.com/TimDettmers/bitsandbytes"
SRC_URI="https://github.com/TimDettmers/bitsandbytes/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"

KEYWORDS="amd64 ~arm64 ~x86"

RESTRICT="test"

PATCHES=(
	"${FILESDIR}/exclude_test.patch"
)

python_compile() {
	CUDA_HOME=/opt/cuda CUDA_VERSION=123 make cuda12x
	distutils-r1_python_compile -j1
}
