# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_USE_SETUPTOOLS=bdepend

inherit distutils-r1

DESCRIPTION="Strips outputs from Jupyter and IPython notebooks"
HOMEPAGE="https://github.com/kynan/nbstripout"
SRC_URI="https://github.com/kynan/nbstripout/archive/refs/tags/${PV}.tar.gz -> ${P}.tar.gz"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~hppa ~ia64 ~x86"
IUSE=""
RESTRICT="test"

RDEPEND="
		dev-python/nbformat
"
