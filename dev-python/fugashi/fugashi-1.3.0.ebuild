# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTIS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 pypi

DESCRIPTION="A Cython MeCab wrapper for fast, pythonic Japanese tokenization"
HOMEPAGE="https://github.com/polm/fugashi"
SRC_URI="$(pypi_sdist_url)"
LICENSE="MIT"
SLOT="0"

KEYWORDS="amd64 ~hppa ~ia64 ~x86"
IUSE="test"

RESTRICT="!test? ( test )"

RDEPEND="
"
