# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit python-r1 distutils-r1 pypi
DESCRIPTION="Parameter-Efficient Fine-Tuning"
HOMEPAGE="https://github.com/huggingface/peft"
SRC_URI="$(pypi_sdist_url)"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="amd64 ~x86 ~arm ~arm64"
IUSE="doc"
