# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTIS_EXT=1
DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit distutils-r1 pypi

DESCRIPTION="A small version of UniDic packaged for Python"
HOMEPAGE="https://github.com/polm/unidic-lite"
SRC_URI="$(pypi_sdist_url --no-normalize)"
S=${WORKDIR}/${P/_/-}

LICENSE="MIT"
SLOT="0"

KEYWORDS="amd64 ~hppa ~ia64 ~x86"
IUSE="test"

RESTRICT="!test? ( test )"

RDEPEND="
"
