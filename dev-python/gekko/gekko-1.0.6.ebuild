# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_EXT=1
inherit python-r1 distutils-r1

DESCRIPTION="Machine learning and optimization for dynamic systems"
HOMEPAGE="https://github.com/BYU-PRISM/GEKKO"
SRC_URI="https://github.com/BYU-PRISM/GEKKO/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
MY_PN="GEKKO"
MY_P="${MY_PN}-${PV}"
S="${WORKDIR}/${MY_P}"

LICENSE="MIT"
SLOT="0"

KEYWORDS="amd64 ~arm64 ~hppa ~ia64 ~x86"

RESTRICT="test"
