# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
inherit python-r1 distutils-r1 pypi
DESCRIPTION="Python framework for fast Vector Space Modelling"
HOMEPAGE="https://pypi.python.org/pypi/gensim"
SRC_URI="$(pypi_sdist_url)"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86 ~arm ~arm64"
IUSE="doc"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]
		>=dev-python/smart_open-1.8.1
		>=dev-python/scipy-0.18.1
		>=dev-python/numpy-1.11.3"

RDEPEND=""
