# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
PYPI_NO_NORMALIZE=true
inherit python-r1 distutils-r1 pypi
DESCRIPTION="Flashlight Text bindings for Python"
HOMEPAGE="https://github.com/flashlight/text"
SRC_URI="https://github.com/flashlight/text/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
MY_P="text-${PV}"
S="${WORKDIR}/${MY_P}"

LICENSE="MIT"
SLOT="0"
KEYWORDS="amd64 ~x86 ~arm ~arm64"
IUSE="doc"

DEPEND="dev-python/setuptools[${PYTHON_USEDEP}]"

RDEPEND=""

PATCHES=(
	"${FILESDIR}/setup.py.patch"
)

python_compile() {
	export USE_KENLM=0
	distutils-r1_python_compile
}
