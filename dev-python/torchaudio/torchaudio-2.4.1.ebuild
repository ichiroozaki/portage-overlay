# Copyright 1999-2024 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

DISTUTILS_USE_PEP517=setuptools
PYTHON_COMPAT=( python3_{10..12} )
DISTUTILS_SINGLE_IMPL=1
DISTUTILS_EXT=1
inherit cuda distutils-r1

DESCRIPTION="An audio package for PyTorch"
HOMEPAGE="https://github.com/pytorch/audio"
SRC_URI="https://github.com/pytorch/audio/archive/refs/tags/v${PV}.tar.gz
		-> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"

KEYWORDS="amd64"

RESTRICT="test"

RDEPEND="
	=sci-libs/pytorch-${PV}*[${PYTHON_SINGLE_USEDEP}]
	media-sound/sox
	media-video/ffmpeg
"

S="${WORKDIR}/audio-${PV}"

python_compile() {
	addpredict "/proc/self/task"

	export CC="$(cuda_gccdir)/gcc"
	export CXX="$(cuda_gccdir)/g++"
	export CUDAHOSTCXX="$(cuda_gccdir)/g++"
	export BUILD_SOX=OFF
	export USE_CUDA=ON
	export TORCH_CUDA_ARCH_LIST="${TORCH_CUDA_ARCH_LIST:-3.5 7.0}"
	export TORCH_INSTALL_PREFIX="$(python_get_sitedir)/torch"
	export FFMPEG_ROOT=/usr

	distutils-r1_python_compile
}
